import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'

const app = express()
const port = process.env.PORT || 8000

app.use(morgan('tiny'))
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (_, res) => res.send({ msg: 'Hello World' }))

app.listen(port, () => console.log('Server on port', port))
